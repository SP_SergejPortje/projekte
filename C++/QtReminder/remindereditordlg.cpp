#include "remindereditordlg.h"
#include "ui_remindereditordlg.h"

ReminderEditorDlg::ReminderEditorDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReminderEditorDlg)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());
}

ReminderEditorDlg::~ReminderEditorDlg()
{
    delete ui;
}

int ReminderEditorDlg::getHour()
{
    return ui->sbHour->value();
}

int ReminderEditorDlg::getMinute()
{
    return ui->sbMin->value();
}

int ReminderEditorDlg::getSecond()
{
    return ui->sbSec->value();
}

bool ReminderEditorDlg::getActive()
{
    return ui->cbActive->checkState();
}

bool ReminderEditorDlg::getRepeat()
{
    return ui->cbRepeat->checkState();
}

QString ReminderEditorDlg::getReminderMsg()
{
    return ui->leMessage->text();
}

void ReminderEditorDlg::edit(const int &hour, const int &minute, const int &second, const bool &active, const bool &repeat, const QString &message)
{
    ui->sbHour->setValue(hour);
    ui->sbMin->setValue(minute);
    ui->sbSec->setValue(second);
    ui->cbActive->setChecked(active);
    ui->cbRepeat->setChecked(repeat);
    ui->leMessage->setText(message);
}
