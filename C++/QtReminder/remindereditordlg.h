#ifndef REMINDEREDITORDLG_H
#define REMINDEREDITORDLG_H

#include <QDialog>

namespace Ui {
class ReminderEditorDlg;
}

class ReminderEditorDlg : public QDialog
{
    Q_OBJECT

public:
    explicit ReminderEditorDlg(QWidget *parent = nullptr);
    ~ReminderEditorDlg();

    int getHour();
    int getMinute();
    int getSecond();
    bool getActive();
    bool getRepeat();
    QString getReminderMsg();

    void edit(const int& hour, const int& minute, const int& second,
              const bool& active, const bool& repeat, const QString& message);

private:
    Ui::ReminderEditorDlg *ui;

public slots:
    void on_bBox_accepted() { accept(); }
    void on_bBox_rejected() { reject(); }
};

#endif // REMINDEREDITORDLG_H
