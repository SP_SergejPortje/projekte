#ifndef REMINDER_H
#define REMINDER_H

#include <QObject>
#include <QTimer>
#include <QTime>
#include <QMessageBox>

class Reminder : public QObject
{
    Q_OBJECT
public:
    explicit Reminder(QObject *parent = nullptr);
    ~Reminder();
    QString getRemainingTime();
    void reset();

    QString getCountdown() const;
    void setCountdown(const QTime &value);

    bool getActive() const;
    void setActive(bool value);

    bool getRepeat() const;
    void setRepeat(bool value);

    QString getReminder() const;
    void setReminder(const QString &value);

    void beginEditing();
    void endEditing();

private:
    QString reminder;
    QTime countdown;
    bool active;
    bool repeat;
    QTime tempTime;
    QTimer *timer;
    bool firstRun;
    QSharedPointer<QMessageBox> msg;

signals:
    void countdownChanged();
    void activeChanged();

private slots:
    void updateCountdown();
    void updateState();
};

#endif // REMINDER_H
