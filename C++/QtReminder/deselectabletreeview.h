#ifndef DESELECTABLETREEVIEW_H
#define DESELECTABLETREEVIEW_H

#include <QTreeView>
#include <QMouseEvent>

class DeselectableTreeView : public QTreeView
{
public:
    DeselectableTreeView(QWidget *parent) : QTreeView(parent) {}
    virtual ~DeselectableTreeView() {}

private:
    virtual void mousePressEvent(QMouseEvent *event)
    {
        QModelIndex item = indexAt(event->pos());
        bool selected = selectionModel()->isSelected(item);
        QTreeView::mousePressEvent(event);
        if ((selected || (item.row() == -1 && item.column() == -1)) && event->button() == Qt::LeftButton)
            clearSelection();
    }
};

#endif // DESELECTABLETREEVIEW_H
