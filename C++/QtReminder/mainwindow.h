#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <QItemSelectionModel>
#include <QContextMenuEvent>
#include <QAction>
#include <QTime>
#include <QMenu>
#include "deselectabletreeview.h"
#include "remindereditordlg.h"
#include "remindermodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    ReminderModel *m_model;
    DeselectableTreeView *treeView;
    QAction *addAct;
    QAction *delAct;
    QAction *editAct;
    void customContextMenu(QPoint pos);
    void insertData(int hour, int minute, int second, bool active,
                    bool repeat, QString msg);

private slots:
    void addData();
    void deleteData();
    void editData();
};

#endif // MAINWINDOW_H
