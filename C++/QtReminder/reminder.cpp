#include "reminder.h"

Reminder::Reminder(QObject *parent) :
    QObject(parent),
    timer(new QTimer(this))
{
    active = false;
    repeat = false;
    firstRun = true;
    countdown = QTime(0,0,1);
    tempTime = QTime(0,0,0);
    reminder = "";
    timer->setInterval(1000);
    connect(timer, &QTimer::timeout, this, &Reminder::updateCountdown);
}

Reminder::~Reminder()
{
}

QString Reminder::getRemainingTime()
{
    return tempTime.toString("HH:mm:ss");
}

void Reminder::reset()
{
    tempTime = countdown;
    emit countdownChanged();
}

QString Reminder::getCountdown() const
{
    return countdown.toString("HH:mm:ss");
}

void Reminder::setCountdown(const QTime &value)
{
    countdown = value;
    reset();
}

bool Reminder::getActive() const
{
    return active;
}

void Reminder::setActive(bool value)
{
    active = value;
    if (tempTime == QTime(0,0,0)) reset();
    if (!repeat && !firstRun) firstRun = true;
    updateState();
}

bool Reminder::getRepeat() const
{
    return repeat;
}

void Reminder::setRepeat(bool value)
{
    repeat = value;
    if (tempTime == QTime(0,0,0)) reset();
    updateState();
}

QString Reminder::getReminder() const
{
    return reminder;
}

void Reminder::setReminder(const QString &value)
{
    reminder = value;
}

void Reminder::beginEditing()
{
    timer->stop();
}

void Reminder::endEditing()
{
    if (tempTime != QTime(0,0,0) && active)
        timer->start();
}

void Reminder::updateCountdown()
{
    tempTime = tempTime.addSecs(-1);
    emit countdownChanged();

    if(tempTime == QTime(0,0,0))
    {
        if (firstRun) firstRun = false;
        timer->stop();
        msg = QSharedPointer<QMessageBox>(new QMessageBox());
        msg->setText(reminder);
        msg->setWindowTitle("Reminder");
        msg->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::Tool);
        if (msg->exec())
        {
            if (repeat) reset();
            updateState();
        }
    }
}

void Reminder::updateState()
{
    if ((active && repeat) || (active && !repeat && firstRun))
    {
        timer->start();
    }
    else
    {
        timer->stop();
        active = false;
        emit activeChanged();
    }
}
