#include "remindermodel.h"

ReminderModel::ReminderModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

int ReminderModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_Data.count();
}

int ReminderModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 5;
}

QVariant ReminderModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_Data.size())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        if (index.column() == 0)
            return m_Data[index.row()]->getRemainingTime();
        if (index.column() == 1)
            return m_Data[index.row()]->getCountdown();
        if (index.column() == 2)
            return m_Data[index.row()]->getActive();
        if (index.column() == 3)
            return m_Data[index.row()]->getRepeat();
        if (index.column() == 4)
            return m_Data[index.row()]->getReminder();
    }
    else
        return QVariant();
    return QVariant();
}

QVariant ReminderModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        switch (section)
        {
        case 0:
            return QStringLiteral("Remaining Time");
        case 1:
            return QStringLiteral("Timer");
        case 2:
            return QStringLiteral("Active");
        case 3:
            return QStringLiteral("Repeat");
        case 4:
            return QStringLiteral("Reminder");
        default:
            return QVariant();
        }
    }
    return QVariant();
}

bool ReminderModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        if (index.column() == 1)
            m_Data[index.row()]->setCountdown(value.toTime());
        if (index.column() == 2)
            m_Data[index.row()]->setActive(value.toBool());
        if (index.column() == 3)
            m_Data[index.row()]->setRepeat(value.toBool());
        if (index.column() == 4)
            m_Data[index.row()]->setReminder(value.toString());

        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags ReminderModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool ReminderModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginInsertRows(QModelIndex(), row, row + count - 1);

    for (int i=0; i < count; ++i)
    {
        QSharedPointer<Reminder> ptr = QSharedPointer<Reminder>(new Reminder());
        connect(ptr.data(), &Reminder::countdownChanged, this, &ReminderModel::updateRemainingTime);
        connect(ptr.data(), &Reminder::activeChanged, this, &ReminderModel::updateActive);
        m_Data.insert(row, ptr);
    }

    endInsertRows();
    return true;
}

bool ReminderModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), row, row + count - 1);

    for (int i=0; i < count; ++i)
        m_Data.removeAt(row);

    endRemoveRows();
    return true;
}

void ReminderModel::beginEditing(int row)
{
    m_Data.at(row)->beginEditing();
}

void ReminderModel::endEditing(int row)
{
    m_Data.at(row)->endEditing();
}

void ReminderModel::updateRemainingTime()
{
    auto it = std::find(m_Data.begin(), m_Data.end(), sender());
    if (it != m_Data.end())
    {
        int row = std::distance(m_Data.begin(), it);
        QModelIndex index = createIndex(row,0);
        emit dataChanged(index, index, {Qt::DisplayRole});
    }
}

void ReminderModel::updateActive()
{
    auto it = std::find(m_Data.begin(), m_Data.end(), sender());
    if (it != m_Data.end())
    {
        int row = std::distance(m_Data.begin(), it);
        QModelIndex index = createIndex(row,2);
        emit dataChanged(index, index, {Qt::DisplayRole});
    }
}

