#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_model(new ReminderModel(this)),
    treeView(new DeselectableTreeView(this))
{
    ui->setupUi(this);
    this->setWindowTitle("SimPleReminder");
    this->setFixedWidth(this->width());
    treeView->setRootIsDecorated(false);
    treeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    treeView->setSelectionMode(QAbstractItemView::SingleSelection);
    treeView->setFocusPolicy(Qt::NoFocus);
    treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    treeView->setSortingEnabled(true);
    connect(treeView, &QAbstractItemView::customContextMenuRequested, this, &MainWindow::customContextMenu);

    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
    proxyModel->setSourceModel(m_model);
    treeView->setModel(proxyModel);

    addAct = new QAction(tr("&Add"), this);
    connect(addAct, &QAction::triggered, this, &MainWindow::addData);
    delAct = new QAction(tr("&Delete"), this);
    connect(delAct, &QAction::triggered, this, &MainWindow::deleteData);
    editAct = new QAction(tr("&Edit"), this);
    connect(editAct, &QAction::triggered, this, &MainWindow::editData);

    ui->verticalLayout->addWidget(treeView);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addData()
{
    ReminderEditorDlg dlg(this);
    dlg.setWindowTitle(tr("Add a Reminder"));

    if (dlg.exec()) insertData(dlg.getHour(), dlg.getMinute(), dlg.getSecond(), dlg.getActive(),
                               dlg.getRepeat(), dlg.getReminderMsg());
}

void MainWindow::insertData(int hour, int minute, int second, bool active, bool repeat, QString msg)
{
    m_model->insertRows(0,1,QModelIndex());

    QTime countdown(hour,minute,second);
    QModelIndex index = m_model->index(0,1,QModelIndex());
    if (countdown != QTime(0,0,0)) m_model->setData(index, countdown, Qt::EditRole);
    index = m_model->index(0,2,QModelIndex());
    m_model->setData(index, active, Qt::EditRole);
    index = m_model->index(0,3,QModelIndex());
    m_model->setData(index, repeat, Qt::EditRole);
    index = m_model->index(0,4,QModelIndex());
    m_model->setData(index, msg, Qt::EditRole);
}

void MainWindow::deleteData()
{
    QSortFilterProxyModel *proxy = static_cast<QSortFilterProxyModel*>(treeView->model());
    QItemSelectionModel *selectionModel = static_cast<QItemSelectionModel*>(treeView->selectionModel());

    const QModelIndexList indexes = selectionModel->selectedRows();

    for (QModelIndex index : indexes)
    {
        int row = proxy->mapToSource(index).row();
        m_model->removeRows(row, 1, QModelIndex());
    }
}

void MainWindow::editData()
{
    QSortFilterProxyModel *proxyModel = static_cast<QSortFilterProxyModel*>(treeView->model());
    QItemSelectionModel *selection = treeView->selectionModel();

    const QModelIndexList indexes = selection->selectedRows();
    QTime countdown;
    bool active, repeat;
    QString reminderMsg;
    int row = -1;

    for (const QModelIndex& index : indexes)
    {
        row = proxyModel->mapToSource(index).row();
        QModelIndex countdownIndex = m_model->index(row, 1, QModelIndex());
        QModelIndex activeIndex = m_model->index(row, 2, QModelIndex());
        QModelIndex repeatIndex = m_model->index(row, 3, QModelIndex());
        QModelIndex reminderMsgIndex = m_model->index(row, 4, QModelIndex());

        countdown = m_model->data(countdownIndex, Qt::DisplayRole).toTime();
        active = m_model->data(activeIndex, Qt::DisplayRole).toBool();
        repeat = m_model->data(repeatIndex, Qt::DisplayRole).toBool();
        reminderMsg = m_model->data(reminderMsgIndex, Qt::DisplayRole).toString();
    }

    m_model->beginEditing(row);
    ReminderEditorDlg dlg(this);
    dlg.setWindowTitle(tr("Edit Reminder"));
    dlg.edit(countdown.hour(),countdown.minute(),countdown.second(),
             active, repeat, reminderMsg);

    if (dlg.exec())
    {
        const QTime newCountdown = QTime(dlg.getHour(),dlg.getMinute(),dlg.getSecond());
        const bool newActive = dlg.getActive();
        const bool newRepeat = dlg.getRepeat();
        const QString newReminderMsg = dlg.getReminderMsg();

        if (newCountdown != countdown) {
            const QModelIndex index = m_model->index(row,1,QModelIndex());
            m_model->setData(index, newCountdown, Qt::EditRole);
        }
        if (newActive != active) {
            const QModelIndex index = m_model->index(row,2,QModelIndex());
            m_model->setData(index,newActive,Qt::EditRole);
        }
        if (newRepeat != repeat) {
            const QModelIndex index = m_model->index(row,3,QModelIndex());
            m_model->setData(index,newRepeat,Qt::EditRole);
        }
        if (QString::compare(newReminderMsg,reminderMsg)) {
            const QModelIndex index = m_model->index(row,4,QModelIndex());
            m_model->setData(index,newReminderMsg,Qt::EditRole);
        }
    }
    m_model->endEditing(row);
}

void MainWindow::customContextMenu(QPoint pos)
{
    QModelIndex index = treeView->indexAt(pos);

    QMenu menu(this);
    menu.addAction(addAct);
    menu.addAction(editAct);
    menu.addAction(delAct);

    if (!index.isValid())
    {
        delAct->setEnabled(false);
        editAct->setEnabled(false);
    }
    else
    {
        delAct->setEnabled(true);
        editAct->setEnabled(true);
    }
    menu.exec(treeView->viewport()->mapToGlobal(pos));
}
