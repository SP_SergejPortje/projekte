#ifndef REMINDERMODEL_H
#define REMINDERMODEL_H

#include <QAbstractTableModel>
#include <QSharedPointer>
#include <QModelIndex>
#include <QVector>
#include <QTimer>
#include <stdlib.h>
#include "reminder.h"

class ReminderModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ReminderModel(QObject *parent = nullptr);

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);

    void beginEditing(int row);
    void endEditing(int row);

private:
    QVector<QSharedPointer<Reminder>> m_Data;

private slots:
    void updateRemainingTime();
    void updateActive();
};

#endif // REMINDERMODEL_H
