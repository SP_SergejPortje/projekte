import pytest

from app import databaseClasses


@pytest.mark.usefixtures("testapp")
class TestURLs:

    def test_verfuegbare_seminare(self, testapp):
        assert self.hat_login_weiterleitung(testapp, "/verfuegbareSeminare")
        assert self.login(testapp, "lena", "12345")
        assert self.seite_okay(testapp, "/verfuegbareSeminare")

    def test_angemeldete_seminare(self, testapp):
        assert self.hat_login_weiterleitung(testapp, "/angemeldeteSeminare")
        assert self.login(testapp, "lena", "12345")
        assert self.seite_okay(testapp, "/angemeldeteSeminare")

    def test_anmelden(self, testapp):
        assert self.hat_login_weiterleitung(testapp, "/anmelden?id=" + 
                                            str(databaseClasses.Zeitraum.objects().first().id))
        assert self.login(testapp, "lena", "12345")
        assert self.seite_okay(testapp, "/anmelden?id=" + str(databaseClasses.Zeitraum.objects().first().id))
        assert self.seite_fehler(testapp, "/anmelden?id=FALSCHE-ID")

    def test_abmelden(self, testapp):
        assert self.hat_login_weiterleitung(testapp, "/abmelden?id=" +
                                            str(databaseClasses.Zeitraum.objects().first().id))
        assert self.login(testapp, "lena", "12345")
        assert self.seite_okay(testapp, "/abmelden?id=" + str(databaseClasses.Zeitraum.objects().first().id))
        assert self.seite_fehler(testapp, "/abmelden?id=FALSCHE-ID")

    @staticmethod
    def hat_login_weiterleitung(testapp, location):
        seite = testapp.get(location, follow_redirects=False)
        return seite.status_code == 302 and seite.location.startswith("http://localhost/login")

    @staticmethod
    def login(testapp, user, pw):
        seite = testapp.post("/login", data=dict(username=user, password=pw))
        return seite.status_code == 302 and seite.location == "http://localhost/verfuegbareSeminare"

    @staticmethod
    def seite_okay(testapp, location):
        seite = testapp.get(location, follow_redirects=True)
        return seite.status_code == 200

    @staticmethod
    def seite_fehler(testapp, location):
        seite = testapp.get(location, follow_redirects=True)
        return seite.status_code == 404
