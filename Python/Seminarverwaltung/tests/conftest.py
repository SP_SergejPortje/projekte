import pytest

from app import config
from app.factory import create_app

@pytest.fixture()
def testapp(request):
    app = create_app(config.TestingConfig)
    client = app.test_client()

    return client