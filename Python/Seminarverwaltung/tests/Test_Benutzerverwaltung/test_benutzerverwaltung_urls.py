import pytest
from app import databaseClasses

#Url Tests für die Benutzerverwaltung
@pytest.mark.usefixtures("testapp")
class TestURLs:

# Nicht Eingeloggt------------------------------------------------------------------------------------------------------
    # Test ob Benutzerliste geladen wird wenn nicht eingeloggt oder ob Redirected wird
    def test_Benutzerliste(self, testapp):
        rv = testapp.get('/benutzerliste', follow_redirects=False)
        assert rv.status_code == 302

    # Test ob Benutzer anlegen geladen wird wenn nicht eingeloggt oder ob Redirected wird
    def test_Benutzeranlgen(self, testapp):
        rv = testapp.get('/benutzeranlegen', follow_redirects=False)
        assert rv.status_code == 302

    # Test ob Benutzer bearbeiten geladen wird wenn nicht eingeloggt ohne ID oder ob Redirected wird
    def test_Benutzerbearbeiten_Without_ID(self, testapp):
        rv = testapp.get('/benutzerbearbeiten', follow_redirects=False)
        assert rv.status_code == 302

    # Test ob Benutzer bearbeiten geladen wird wenn nicht eingeloggt mit ID oder ob Redirected wird
    def test_Benutzerbearbeiten_With_ID(self, testapp):
        peter = databaseClasses.Benutzer.objects(username="peter").get()
        rv = testapp.get('/benutzerbearbeiten?id=' + str(peter.id), follow_redirects=False)
        assert rv.status_code == 302

# Eingeloggt als Admin--------------------------------------------------------------------------------------------------
    # Test ob Benutzerliste nach einloggen als Admin aufgerufen werden kann
    def test_Benutzerliste_Logged_In_As_Admin(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/benutzerliste')
        assert rv.status_code == 200

    # Test ob Benutzer anlgen nach einloggen als Admin aufgerufen werden kann
    def test_Benutzeranlegen_Logged_In_As_Admin(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/benutzeranlegen')
        assert rv.status_code == 200

    # Test ob Benutzer bearbeiten nach einloggen als Admin aber ohne User ID aufgerufen werden kann
    def test_Benutzerbearbeiten_Logged_In_As_Admin_Without_User_ID(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/benutzerbearbeiten')
        assert rv.status_code == 404

    # Test ob Benutzer bearbeiten nach einloggen als Admin und mit User ID aufgerufen werden kann
    def test_Benutzerbearbeiten_Logged_In_As_Admin_With_User_ID(self, testapp):
        peter = databaseClasses.Benutzer.objects(username="peter").get()
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/benutzerbearbeiten?id=' + str(peter.id))
        assert rv.status_code == 200

# Eingeloggt als User, Redirect-----------------------------------------------------------------------------------------
    # Test ob Benutzerliste angezeigt aufgerufen werden kann nach einloggen als User oder Redirected wird
    def test_Benutzerliste_Logged_In_As_User(self, testapp):
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzerliste')
        assert rv.status_code == 302

    # Test ob Benutzer anlgen aufgerufen werden kann nach einloggen als User oder Redirected wird
    def test_Benutzeranlegen_Logged_In_As_User(self, testapp):
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzeranlegen')
        assert rv.status_code == 302

    # Test ob Benutzer bearbeiten aufgerufen werden kann nach einloggen als User ohne ID oder Redirected wird
    def test_Benutzerbearbeiten_Logged_In_As_User_Without_ID(self, testapp):
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzerbearbeiten')
        assert rv.status_code == 302

    # Test ob Benutzer bearbeiten aufgerufen werden kann nach einloggen als User mit ID oder Redirected wird
    def test_Benutzerbearbeiten_Logged_In_As_User_With_ID(self, testapp):
        peter = databaseClasses.Benutzer.objects(username="peter").get()
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzerbearbeiten?id=' + str(peter.id))
        assert rv.status_code == 302

# Eingeloggt als User, Meldung------------------------------------------------------------------------------------------
    # Test ob Meldung angezeigt wird wenn User auf Benutzerliste zugreifen will
    def test_Benutzerliste_Logged_In_As_User_Message(self, testapp):
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzerliste', follow_redirects=True)
        assert b"Zugriff nur f" and b"r Admins" in rv.data

    # Test ob Meldung angezeigt wird wenn User auf Benutzer anlegen zugreifen will
    def test_Benutzeranlegen_Logged_In_As_User_Message(self, testapp):
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzeranlegen', follow_redirects=True)
        assert b"Zugriff nur f" and b"r Admins" in rv.data

    # Test ob Meldung angezeigt wird wenn User auf Benutzer bearbeiten zugreifen will
    def test_Benutzerbearbeiten_Logged_In_As_User_Without_User_ID_Message(self, testapp):
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzerbearbeiten', follow_redirects=True)
        assert b"Zugriff nur f" and b"r Admins" in rv.data

    # Test ob Meldung angezeigt wird wenn User auf Benutzer bearbeiten mit ID zugreifen will
    def test_Benutzerbearbeiten_Logged_In_As_User_With_User_ID_Message(self, testapp):
        peter = databaseClasses.Benutzer.objects(username="peter").get()
        rv = testapp.post('/login', data=dict(username='lena', password='12345'), follow_redirects=True)
        rv = testapp.get('/benutzerbearbeiten?id=' + str(peter.id), follow_redirects=True)
        assert b"Zugriff nur f" and b"r Admins" in rv.data
