import pytest

@pytest.mark.usefixtures("testapp")
class TestURLs:

# Nicht Eingeloggt------------------------------------------------------------------------------------------------------
    def test_Seminarliste(self, testapp):
        rv = testapp.get('/seminarliste', follow_redirects=False)
        assert rv.status_code == 302

    def test_edit(self, testapp):
        rv = testapp.get('/edit', follow_redirects=False)
        assert rv.status_code == 302

    def test_semuser(self, testapp):
        rv = testapp.get('/semuser?id=5d01f7e57c49a01f87135233', follow_redirects=False)
        assert rv.status_code == 302

    def test_add(self, testapp):
        rv = testapp.get('/add', follow_redirects=False)
        assert rv.status_code == 302
# Eingeloggt als Admin--------------------------------------------------------------------------------------------------
    def test_Seminarliste(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/seminarliste', follow_redirects=False)
        assert rv.status_code == 302

    def test_edit(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/edit', follow_redirects=False)
        assert rv.status_code == 302

    def test_semuser(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/semuser?id=5d01f7e57c49a01f87135233', follow_redirects=False)
        assert rv.status_code == 302

    def test_add(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/add', follow_redirects=False)
        assert rv.status_code == 302
# Eingeloggt als Admin, Inhalt-----------------------------------------------------------------------------------------
    def test_Seminarliste(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/seminarliste', follow_redirects=False)
        assert b"Seminare" in rv.data

    def test_edit(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/edit', follow_redirects=False)
        assert b"Seminar bearbeiten" in rv.data

    def test_semuser(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/semuser?id=5d01f7e57c49a01f87135233', follow_redirects=False)
        assert b"Teilnehmer" in rv.data

    def test_add(self, testapp):
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/add', follow_redirects=False)
        assert b"Anlegen" in rv.data