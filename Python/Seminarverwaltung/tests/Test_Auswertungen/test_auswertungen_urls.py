import pytest

@pytest.mark.usefixtures("testapp")
class TestURLs:
    def test_aswrtBenutzerProSeminar(self, testapp):
        """ Teste ob die Seite aswrtBenutzerProSeminar lädt und redirected """
        rv = testapp.get("/aswrtBenutzerProSeminar")
        assert rv.status_code == 302

    def test_aswrtAuslastungProSeminar(self, testapp):
        """ Teste ob die Seite aswrtAuslastungProSeminar lädt und redirected """
        rv = testapp.get("/aswrtAuslastungProSeminar")
        assert rv.status_code == 302

    def test_aswrtBelSeminareProBenutzer(self, testapp):
        """ Teste ob die Seite aswrtBelSeminareProBenutzer lädt und redirected """
        rv = testapp.get("/aswrtBelSeminareProBenutzer")
        assert  rv.status_code == 302

# ----------------------------------------------------------------------------------------------------------------------

    def test_aswrtBenutzerProSeminar_Not_Loged_In(self, testapp):
        """ Teste ob fehlermeldung angezeit wird wenn nich Eingeloggt """
        rv = testapp.get("/aswrtBenutzerProSeminar", follow_redirects=True)
        assert b"Bitte einloggen!" in rv.data

    def test_aswrtAuslastungProSeminar_Not_Loged_In(self, testapp):
        """ Teste ob fehlermeldung angezeit wird wenn nicht Eingeloggt """
        rv = testapp.get("/aswrtAuslastungProSeminar", follow_redirects=True)
        assert b"Bitte einloggen!" in rv.data

    def test_aswrtBelSeminareProBenutzer_Not_Loged_In(self, testapp):
        """ Teste ob fehlermeldung angezeit wird wenn nicht Eingeloggt """
        rv = testapp.get("/aswrtBelSeminareProBenutzer", follow_redirects=True)
        assert b"Bitte einloggen!" in rv.data

# ----------------------------------------------------------------------------------------------------------------------

    def test_aswrtBenutzerProSeminar_Loged_In(self, testapp):
        """ Teste ob fehlermeldung angezeit wird wenn kein Admin """
        rv = testapp.post("/login", data=dict(
            username="hans",
            password="12345"
        ), follow_redirects=True)

        rv = testapp.get("/aswrtBenutzerProSeminar", follow_redirects=True)
        assert b"zugriff nur f" and  b"Admins" in rv.data

    def test_aswrtAuslastungProSeminar_Loged_In(self, testapp):
        """ Teste ob fehlermeldung angezeigt wird wenn kein Admin """
        rv = testapp.post("/login", data=dict(
            username="hans",
            password="12345"
        ), follow_redirects=True)

        rv = testapp.get("/aswrtAuslastungProSeminar", follow_redirects=True)
        assert b"zugriff nur f" and  b"Admins" in rv.data

    def test_aswrtBelSeminareProBenutzer_Loget_In(self, testapp):
        """ Teste ob fehlermeldung angezeit wird wenn kein Admin """
        rv = testapp.post("/login", data=dict(
            username="hans",
            password="12345"
        ), follow_redirects=True)

        rv = testapp.get("/aswrtBelSeminareProBenutzer", follow_redirects=True)
        assert b"zugriff nur f" and  b"Admins" in rv.data

# ----------------------------------------------------------------------------------------------------------------------

    def test_aswrtBenutzerProSeminar_Loged_In_As_Admin(self, testapp):
        """ Teste ob status_code 200 als admin zurückkommt """
        rv = testapp.post("/login", data=dict(
            username="admin",
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get("/aswrtBenutzerProSeminar")
        assert rv.status_code == 200

    def test_aswrtAuslastungProSeminar_Loged_In_As_Admin(self, testapp):
        """ Teste ob status_code 200 als admin zurückkommt """
        rv = testapp.post("/login", data=dict(
            username="admin",
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get("/aswrtAuslastungProSeminar")
        assert rv.status_code == 200

    def test_aswrtBelSeminareProBenutzer_Loget_In_As_Admin(self, testapp):
        """ Teste ob status code 200 als admin zurückkommt """
        rv = testapp.post("/login", data=dict(
            username="admin",
            password="admin"
        ), follow_redirects=True)

        rv = testapp.get("/aswrtBelSeminareProBenutzer")
        assert rv.status_code == 200

# ----------------------------------------------------------------------------------------------------------------------

    def test_aswrtBenutzerProSeminar_Admin_Post(self, testapp):
        """ Teste ob erfolgreich an die Seite gepostet wird  """
        rv = testapp.post("/login", data=dict(
            username="admin",
            password="admin"
        ), follow_redirects=True)

        rv = testapp.post("aswrtBenutzerProSeminar", data=dict(yearField="2014"))
        assert rv.status_code == 200
        assert b"2014" in rv.data

    def test_aswrtAuslastungProSeminar_Admin_Post(self, testapp):
        """ Teste ob erfolgreich an die Seite gepostet wird """
        rv = testapp.post("/login", data=dict(
            username="admin",
            password="admin"
        ), follow_redirects=True)

        rv = testapp.post("aswrtAuslastungProSeminar", data=dict(yearField="2014"))
        assert rv.status_code == 200
        assert b"2014" in rv.data

# ----------------------------------------------------------------------------------------------------------------------