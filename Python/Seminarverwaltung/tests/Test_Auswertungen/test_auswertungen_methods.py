import pytest
import random
from app.Auswertungen.views import sortObjectList

class testObject(object):
    def __init__(self, key):
        self.key = key

class TestMethods:
    def test_sortObjectList_asc(self):
        """  """
        items = []
        for x in range (0,3):
            item = testObject(random.randint(0,11))
            items.append(item)

        sortObjectList(list=items, key="key", order="asc")
        assert items[0].key <= items[1].key <= items[2].key

    def test_sortObjectList_desc(self):
        """ """
        items = []
        for x in range(0, 3):
            item = testObject(random.randint(0, 11))
            items.append(item)

        sortObjectList(list=items, key="key", order="desc")
        assert items[0].key >= items[1].key >= items[2].key

