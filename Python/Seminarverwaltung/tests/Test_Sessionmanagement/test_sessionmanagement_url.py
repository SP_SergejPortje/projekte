import time

import pytest


@pytest.mark.usefixtures("testapp")
class TestURLs:
    def test_Login(self, testapp):
        """ Tests if the login page loads """
        rv = testapp.get('/login')
        assert rv.status_code == 200

    def test_Login_Valid_User(self, testapp):
        """Tests if Login with valid userdata works"""
        rv = testapp.post('/login', data=dict(username='peter', password='12345'), follow_redirects=True)
        rv = testapp.get('/login')
        assert rv.status_code == 200

    def test_Login_Valid_User_message(self, testapp):
        """Tests if Login message with valid userdata works"""
        rv = testapp.post('/login', data=dict(username='peter', password='12345'), follow_redirects=True)
        assert b"Erfolgreich eingelogged!" in rv.data

    def test_Login_Valid_Admin(self, testapp):
        """Tests if Login with valid admindata works"""
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        assert rv.status_code == 200

    def test_Login_Valid_Admin_message(self, testapp):
        """Tests if Login message with valid admindata works"""
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        assert b"Erfolgreich eingelogged!" in rv.data

    def test_Login_Invalid_Username(self, testapp):
        """Tests if Invalid Username is found"""
        rv = testapp.post('/login', data=dict(username='rabarbarararararararararbarararaa', password='12345'), follow_redirects=False)
        assert rv.status_code == 200

    def test_Login_Invalid_Username_message(self, testapp):
        """Tests if message invalid Username is displayed"""
        rv = testapp.post('/login', data=dict(username='rabarbarararararararararbarararaa', password='12345'), follow_redirects=True)
        assert b"Nutzername nicht vorhanden!"

    def test_Login_Invalid_Password(self, testapp):
        """Tests if message invalid Password is displayed"""
        rv = testapp.post('/login', data=dict(username='peter', password='123456'),follow_redirects=False)
        assert rv.status_code != 302

    def test_Login_Invalid_Password_message(self, testapp):
        """Tests if message invalid Password is displayed"""
        rv = testapp.post('/login', data=dict(username='peter', password='123456'),follow_redirects=True)
        assert b"Passwort falsch"

    def test_Initlogin(self, testapp):
        """ Tests if the login page loads """
        rv = testapp.get('/')
        assert rv.status_code == 200

    def test_Logout_Logged_out(self, testapp):
        """Tests if the logout page loads"""
        rv = testapp.get('/logout', follow_redirects=True)
        assert 200 == rv.status_code

    def test_Logout_Logged_out_message(self, testapp):
        """Tests if the logout already logged out message works"""
        rv = testapp.get('/logout', follow_redirects=True)
        assert b"Sie wurden automatisch ausgelogged"


    def test_Logout_Logged_in_as_admin(self, testapp):
        """Tests if the logout works"""
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/logout', follow_redirects=True)
        assert 200 == rv.status_code


    def test_Logout_Logged_in_as_admin_message_success(self, testapp):
        """Tests if the logout success message works"""
        rv = testapp.post('/login', data=dict(username='admin', password='admin'), follow_redirects=True)
        rv = testapp.get('/logout', follow_redirects=True)
        assert b"Sie haben sich erfolgreich abgemeldet." in rv.data


    def test_Logout_Logged_in_as_user(self, testapp):
        """Tests if the logout works"""
        rv = testapp.post('/login', data=dict(username='peter', password='12345'), follow_redirects=True)
        rv = testapp.get('/logout', follow_redirects=True)
        assert 200 == rv.status_code


    def test_Logout_Logged_in_as_user_message_success(self, testapp):
        """Tests if the logout works"""
        rv = testapp.post('/login', data=dict(username='peter', password='12345'), follow_redirects=True)
        rv = testapp.get('/logout', follow_redirects=True)
        assert b"Sie haben sich erfolgreich abgemeldet." in rv.data




