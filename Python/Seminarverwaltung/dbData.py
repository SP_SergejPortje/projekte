from app.databaseClasses import *
from werkzeug.security import generate_password_hash
from datetime import datetime
from mongoengine import connect

datetimeFormat = "%d.%m.%Y"
dbName = "Seminarverwaltung"

print("---Verbindung zur Datebank " + dbName + " wird hergestellt---")
connect(dbName)

if not Benutzer.objects:
    print("---Keine Benutzer vorhanden---")
    print("---Benutzer werden erstellt---")

    # username, vorname, nachname, gebdat, passwort, angemeldete Seminare, aktiv, admin
    Benutzer(username="admin", vorname="Max", nachname="Mustermann",
             gebdat=datetime.strptime("10.04.1995", datetimeFormat),
             passwort=generate_password_hash("admin"), aktiv=True, admin=True).save()
    Benutzer(username="lena", vorname="Lena", nachname="Musterfrau",
             gebdat=datetime.strptime("22.06.2001", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="paul", vorname="Paul", nachname="Mustermann",
             gebdat=datetime.strptime("14.03.2000", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="sabrina", vorname="Sabrina", nachname="Musterfrau",
             gebdat=datetime.strptime("03.12.1998", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="karl", vorname="Karl", nachname="Mustermann",
             gebdat=datetime.strptime("17.09.2003", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="hans", vorname="Hans", nachname="Mustermann",
             gebdat=datetime.strptime("29.01.2003", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="lukas", vorname="Lukas", nachname="Mustermann",
             gebdat=datetime.strptime("03.05.1997", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="selina", vorname="Selina", nachname="Musterfrau",
             gebdat=datetime.strptime("24.06.2002", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="emili", vorname="Emili", nachname="Musterfrau",
             gebdat=datetime.strptime("13.11.1998", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="peter", vorname="Peter", nachname="Mustermann",
             gebdat=datetime.strptime("20.02.1996", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="julia", vorname="Julia", nachname="Musterfrau",
             gebdat=datetime.strptime("16.04.1999", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=True, admin=False).save()
    Benutzer(username="Samu", vorname="Samuel", nachname="Goodall",
             gebdat=datetime.strptime("01.04.1997", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=False, admin=False, angemdeldeteSeminare=20).save()
    Benutzer(username="Serge", vorname="Sergej", nachname="Portje",
             gebdat=datetime.strptime("23.10.1998", datetimeFormat),
             passwort=generate_password_hash("12345"), aktiv=False, admin=False, angemdeldeteSeminare=40).save()

    print("---Fertig!---")
else:
    print("---Benutzer vorhanden---")

if not Seminar.objects:
    print("---Keine Seminare vorhanden---")
    print("---Seminare werden erstellt---")

    lena = Benutzer.objects(username="lena").get()
    paul = Benutzer.objects(username="paul").get()
    sabrina = Benutzer.objects(username="sabrina").get()
    karl = Benutzer.objects(username="karl").get()
    hans = Benutzer.objects(username="hans").get()
    lukas = Benutzer.objects(username="lukas").get()
    selina = Benutzer.objects(username="selina").get()
    emili = Benutzer.objects(username="emili").get()
    peter = Benutzer.objects(username="peter").get()
    julia = Benutzer.objects(username="julia").get()

    # Mahte Seminar
    mathe_teilnehmer_ztrm1 = [lena.incAngSemi(), sabrina.incAngSemi(), hans.incAngSemi()]

    mathe_teilnehmer_ztrm2 = [lena.incAngSemi(), paul.incAngSemi(), sabrina.incAngSemi(), karl.incAngSemi(),
                              hans.incAngSemi(), lukas.incAngSemi(), selina.incAngSemi(), emili.incAngSemi(),
                              peter.incAngSemi(), julia.incAngSemi()]  # 100% Auslastung

    mathe_teilnhemer_ztrm3 = [peter.incAngSemi(), emili.incAngSemi(), selina.incAngSemi(), lena.incAngSemi(),
                              paul.incAngSemi()]  # 50% Auslastung

    mathe_ztrm1 = Zeitraum(datetime.strptime("15.12.2019", datetimeFormat),
                           datetime.strptime("10.01.2020", datetimeFormat),
                           mathe_teilnehmer_ztrm1).save()  # Silvester Zeitraum
    mathe_ztrm2 = Zeitraum(datetime.strptime("10.03.2020", datetimeFormat),
                           datetime.strptime("10.05.2020", datetimeFormat),
                           mathe_teilnehmer_ztrm2).save()
    mathe_ztrm3 = Zeitraum(datetime.strptime("01.01.2019", datetimeFormat),
                           datetime.strptime("04.02.2019", datetimeFormat),
                           mathe_teilnhemer_ztrm3).save()

    Seminar("Mathe", "Prof.Dr.Gunter", [mathe_ztrm1, mathe_ztrm2, mathe_ztrm3], 10).save()

    # ------------------------------------------------------------------------------------------------------------------

    # Deutsch Seminar
    deutsch_teilnehmer_ztrm1 = [sabrina.incAngSemi(), karl.incAngSemi(), lena.incAngSemi(), lukas.incAngSemi()]

    deutsch_teilnehmer_ztrm2 = [emili.incAngSemi(), lena.incAngSemi()]  # 20% Auslastung

    deutsch_teilnehmer_ztrm3 = []  # 0% Auslastung

    deutsch_ztrm1 = Zeitraum(datetime.strptime("01.02.2020", datetimeFormat),
                             datetime.strptime("12.12.2020", datetimeFormat),
                             deutsch_teilnehmer_ztrm1).save()
    deutsch_ztrm2 = Zeitraum(datetime.strptime("20.06.2019", datetimeFormat),
                             datetime.strptime("01.07.2019", datetimeFormat),
                             deutsch_teilnehmer_ztrm2).save()
    deutsch_ztrm3 = Zeitraum(datetime.strptime("15.02.2018", datetimeFormat),
                             datetime.strptime("29.04.2018", datetimeFormat),
                             deutsch_teilnehmer_ztrm3).save()
    Seminar("Deutsch", "Prof.Dr.Gauß", [deutsch_ztrm1, deutsch_ztrm2, deutsch_ztrm3], 10).save()

    print("---Fertig!---")
else:
    print("---Seminare vorhanden---")
