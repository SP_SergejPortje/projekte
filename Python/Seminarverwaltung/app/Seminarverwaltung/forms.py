from flask_wtf import FlaskForm
from wtforms import fields

class FilterForm(FlaskForm):
    bezFeld = fields.StringField('bezFeld')
    dozentFeld = fields.StringField('dozentFeld')