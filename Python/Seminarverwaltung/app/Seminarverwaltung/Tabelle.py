from flask_table import Table, Col, DatetimeCol, LinkCol

datetimeFormat="dd.MM.yyyy"

class Tabelle(Table):
    classes = ["table", "table-hover", "clickable-row", "sortable"]
    no_items = "noch kein Termin"
    bezeichnung = Col("Bezeichnung")
    dozent = Col("Dozent")
    start = DatetimeCol("Start", datetime_format=datetimeFormat)
    ende = DatetimeCol("Ende", datetime_format=datetimeFormat)
    Teilnehmer = LinkCol("Teilnehmer", 'seminarverwaltung.semuser', url_kwargs=dict(id='id'), attr='Teilnehmer')
    edit = LinkCol("Bearbeiten", 'seminarverwaltung.edit', url_kwargs=dict(id='id'))

class Itm(object):
    def __init__(self, bezeichnung, dozent, start, ende, Teilnehmer, id):
        self.bezeichnung = bezeichnung
        self.dozent = dozent
        self.start = start
        self.ende = ende
        self.Teilnehmer = Teilnehmer
        self.id = id

class UserTabelle(Table):
    classes = ["table", "table-hover", "clickable-row", "sortable"]
    no_items = "Noch kein Benutzer eingtragen"
    username = Col("Username")
    vorname = Col("Vorname")
    nachname = Col("Nachname")
