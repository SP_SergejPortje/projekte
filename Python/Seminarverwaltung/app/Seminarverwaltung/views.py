from flask import Blueprint, render_template, redirect, url_for, request, session, flash
from flask_mongoengine import *
from flask_mongoengine.wtf import model_form
from app.Sessionmanagement.sessionacces import admin_required, login_required
from app.databaseClasses import Seminar, Zeitraum
from app.Seminarverwaltung.Tabelle import Tabelle, Itm, UserTabelle
from app.Seminarverwaltung.forms import FilterForm

seminarverwaltung_bp = Blueprint("seminarverwaltung", __name__, template_folder="Pages")
db = MongoEngine()

@seminarverwaltung_bp.route("/seminarliste", methods = ["GET", "POST"])
@login_required
@admin_required
def seminarliste():
    form = FilterForm()
    entitiesfiltered = Seminar.objects
    if form.is_submitted() and form.bezFeld.data != "":
        entitiesfiltered = entitiesfiltered.filter(bezeichnung__contains=form.bezFeld.data)
    if form.is_submitted() and form.dozentFeld.data != "":
        entitiesfiltered = entitiesfiltered.filter(dozent__contains=form.dozentFeld.data)
    entities = []
    for seminar in entitiesfiltered:
        for zeitraum in seminar.zeitraeume:
            item =Itm(seminar.bezeichnung, seminar.dozent, zeitraum.start, zeitraum.ende, str(len(zeitraum.teilnehmer))+ "/" + str(seminar.maxTeilnehmer), zeitraum.id)
            entities.append(item)
    table = Tabelle(entities)
    return render_template("seminarterminliste.html", title = "Seminarliste", table = table, form = form, info = session)

@seminarverwaltung_bp.route("/edit", methods = ["GET", "POST"])
@login_required
@admin_required
def edit():
    return render_template("edit.html", title = "Bearbeiten", info = session)

@seminarverwaltung_bp.route("/add", methods = ["GET", "POST"])
@login_required
@admin_required
def add():
    hinzufuegen = model_form(Seminar)
    zeit = model_form(Zeitraum, field_args={'start': {'format': '%d.%m.%Y'}, 'ende': {'format': '%d.%m.%Y'}})

    form = hinzufuegen(request.form)
    zform = zeit(request.form)

    if request.method == 'POST':
        if zform.start.data > zform.ende.data:
            flash('Endtermin muss nach Starttermin sein', 'error')
        else:
            zeitraum = Zeitraum(start = zform.start.data, ende = zform.ende.data)
            zeitraum.save()
            seminar = Seminar(form.bezeichnung.data, form.dozent.data, [zeitraum], form.maxTeilnehmer.data)
            seminar.save()
            flash('Seminar wurde erstellt', 'success')
        return redirect(url_for("seminarverwaltung.seminarliste"))
    return render_template("add.html", title = "Seminar hinzufügen", info = session, hinzufuegen = form, zform=zform)

@seminarverwaltung_bp.route("/semuser", methods = ["GET", "POST"])
@login_required
@admin_required
def semuser():
    termin = Zeitraum.objects.get_or_404(id=request.args.get('id'))
    items = termin.teilnehmer
    table=UserTabelle(items)
    return render_template("semuser.html", title = "Bearbeiten", table = table, info = session)