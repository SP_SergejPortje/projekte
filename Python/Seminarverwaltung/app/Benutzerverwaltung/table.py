from flask_table import Table, Col, DatetimeCol, LinkCol, BoolCol

# Tabelle für die Benutzerliste
class Table(Table):
    classes = ["table", "table-hover", "clickable-row", "sortable"]
    username = Col("Username")
    vorname = Col("Vorname")
    nachname = Col("Nachname")
    gebdat = DatetimeCol("Gebdat", datetime_format="dd.MM.yyyy")
    # passwort = Col("Passwort")
    admin = BoolCol("Rolle", yes_display='Admin', no_display='Benutzer')
    aktiv = BoolCol("Status", yes_display='aktiviert', no_display='deaktiviert')
    edit = LinkCol("Bearbeiten", 'benutzerverwaltung.benutzerBearbeiten', url_kwargs=dict(id='id'), column_html_attrs = {'class': 'sorttable_nosort'})