from flask import flash
from flask_wtf import FlaskForm
from wtforms import fields
from flask_mongoengine.wtf import model_form
from app import databaseClasses

# Passwort-Form für das neue Passwort, wird an das Pre-populated Field vererbt
class PasswortForm(FlaskForm):
    newpasswort = fields.StringField('newpasswort')


# Suchfeld für die Benutzerliste
class SuchForm(FlaskForm):
    suchFeld = fields.StringField('suchFeld')


# Form Funktion zum überprüfen ob der bearbeitetet Nutzer der letzte Admin ist
def adminCheck():
    adminCount = databaseClasses.Benutzer.objects(admin=True, aktiv=True).count()
    if (adminCount == 1):
        flash("Letzer Admin, aktion nicht möglich!", "danger")
        return True
    else:
        return False


# Model Form aus DB zum anlegen der Nutzer, extra Field_Args für das Datumsformat
anlegen = model_form(databaseClasses.Benutzer, field_args={'gebdat': {'format': '%d.%m.%Y'}})

# Model Form aus DB zum bearbeiten der Nutzer, extra Field_Args für das Datumsformat
bearbeiten = model_form(model=databaseClasses.Benutzer, base_class=PasswortForm,
                        field_args={'gebdat': {'format': '%d.%m.%Y'}})
