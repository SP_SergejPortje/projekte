from flask import Blueprint, render_template, redirect, url_for, flash, session
from flask import request
from werkzeug.security import generate_password_hash
from app.Benutzerverwaltung.table import Table
from app.Benutzerverwaltung.forms import *
from app.Sessionmanagement.sessionacces import admin_required, login_required


# als Blueprint definieren, Template Folder angeben und Startfunktion angeben
benutzerverwaltung_blueprint = Blueprint('benutzerverwaltung', __name__, template_folder='pages')


# ----------------------------------------------------------------------------------------------------------------------
# Aufbau der Benutzerliste-Seite mit Tabelle aller Benutzer
@benutzerverwaltung_blueprint.route("/benutzerliste", methods=["GET", "POST"])
@login_required
@admin_required
def benutzerliste():
    form = SuchForm()
    if form.is_submitted() and form.suchFeld.data != "":
        items = databaseClasses.Benutzer.objects.filter(username__startswith=form.suchFeld.data)
    else:
        items = databaseClasses.Benutzer.objects
    table = Table(items)
    return render_template('benutzerliste.html', title='Benutzerliste', table=table, form=form, info=session)


# Aufbau der Benutzeranlegen-Seite mit allen nötigen Eingabefeldern
@benutzerverwaltung_blueprint.route("/benutzeranlegen", methods=["GET", "POST"])
@login_required
@admin_required
def benutzerAnlegen():
    form = anlegen(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        benutzer = databaseClasses.Benutzer(username=form.username.data, vorname=form.vorname.data,
                                            nachname=form.nachname.data, gebdat=form.gebdat.data,
                                            passwort=generate_password_hash(form.passwort.data, method='pbkdf2:sha256',
                                                                            salt_length=8), admin=form.admin.data,
                                            aktiv=form.aktiv.data)
        benutzer.save()
        flash('Benutzer wurde erstellt', 'success')
        return redirect(url_for("benutzerverwaltung.benutzerliste"))

    return render_template('benutzerAnlegen.html', title='Benutzer anlegen', anlegen=form, info=session)


# Aufbau der Benutzerbearbeiten-Seite mit allen Eingabefeldern + den schon Vorhandenen Daten
@benutzerverwaltung_blueprint.route("/benutzerbearbeiten", methods=["GET", "POST"])
@login_required
@admin_required
def benutzerBearbeiten():
    benutzerID = request.args.get('id')
    benutzer = databaseClasses.Benutzer.objects.get_or_404(id=benutzerID)
    form = bearbeiten(request.form, obj=benutzer)

    isAdmin = benutzer.admin

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(benutzer)
        if form.newpasswort.data != "":
            benutzer.passwort = generate_password_hash(form.newpasswort.data, method='pbkdf2:sha256', salt_length=8)
        if isAdmin == True and form.admin.data == False:
            benutzer.admin = adminCheck()
        if isAdmin == True and form.aktiv.data == False:
            benutzer.aktiv = adminCheck()
        benutzer.save()
        flash('Benutzer wurde aktualisiert', 'success')
        return redirect(url_for("benutzerverwaltung.benutzerliste"))
    else:
        print(form.errors)
    return render_template('benutzerBearbeiten.html', title='Benutzer bearbeiten', bearbeiten=form, info=session,
                           benutzer=benutzer)
