from datetime import datetime

from flask import Blueprint, render_template, session, request, flash
from app.Sessionmanagement.sessionacces import login_required
from flask_table import Table, Col, DatetimeCol, LinkCol

from app import databaseClasses

seminarbelegung_blueprint = Blueprint("seminarbelegung", __name__, template_folder="Pages")
datetime_format_de = "dd.MM.yyyy"
datetime_format_de_strf = "%d.%m.%Y"


@seminarbelegung_blueprint.route("/verfuegbareSeminare")
@login_required
def verfuegbare_seminare():
    items = []
    for seminar in databaseClasses.Seminar.objects:
        for zeitraum in seminar.zeitraeume:
            if len(zeitraum.teilnehmer) < seminar.maxTeilnehmer and not zeitraum.hat_teilnehmer(session["user_id"])\
                    and (zeitraum.start - datetime.now()).days > 0:
                items.append(Seminartermin(seminar.bezeichnung, seminar.dozent, zeitraum.start, zeitraum.ende,
                                           zeitraum.id))

    items.sort(key=lambda r: r.start)

    # noinspection PyUnresolvedReferences
    return render_template("seminarliste.html", title="Verfügbare Seminare", info=session,
                           table=SeminartabelleAnmelden(items), verf=True)


@seminarbelegung_blueprint.route("/anmelden")
@login_required
def anmelden():
    zeitraum = databaseClasses.Zeitraum.objects.get_or_404(id=request.args.get("id"))
    seminar = databaseClasses.Seminar.objects.get_or_404(zeitraeume=zeitraum.id)
    benutzer = databaseClasses.Benutzer.objects.get_or_404(id=session["user_id"])

    if len(zeitraum.teilnehmer) >= seminar.maxTeilnehmer:
        flash("Maximale Teilnehmerzahl wurde überschritten", "danger")
    elif zeitraum.hat_teilnehmer(session["user_id"]):
        flash("Sie sind bereits für diesen Termin angemeldet", "danger")
    elif (zeitraum.start - datetime.now()).days <= 0:
        flash("Sie können sich nicht für einen vergangenen Termin anmelden", "danger")
    else:
        zeitraum.teilnehmer.append(benutzer)
        zeitraum.save()
        benutzer.incAngSemi()
        flash("Erfolgreich angemeldet", "success")

    return verfuegbare_seminare()


@seminarbelegung_blueprint.route("/angemeldeteSeminare")
@login_required
def angemeldete_seminare():
    items = []
    for seminar in databaseClasses.Seminar.objects:
        for zeitraum in seminar.zeitraeume:
            if zeitraum.hat_teilnehmer(session["user_id"]) and (zeitraum.start - datetime.now()).days > 0:
                items.append(Seminartermin(seminar.bezeichnung, seminar.dozent, zeitraum.start, zeitraum.ende,
                                           zeitraum.id))

    items.sort(key=lambda r: r.start)

    # noinspection PyUnresolvedReferences
    return render_template("seminarliste.html", title="Angemeldete Seminare", info=session,
                           table=SeminartabelleAbmelden(items), verf=False)


@seminarbelegung_blueprint.route("/abmelden")
@login_required
def abmelden():
    zeitraum = databaseClasses.Zeitraum.objects.get_or_404(id=request.args.get("id"))
    seminar = databaseClasses.Seminar.objects.get_or_404(zeitraeume=zeitraum.id)
    benutzer = databaseClasses.Benutzer.objects.get_or_404(id=session["user_id"])

    if request.args.get("best") == "ja":
        if not zeitraum.hat_teilnehmer(session["user_id"]):
            flash("Sie sind nicht für diesen Termin angemeldet", "danger")
        elif (zeitraum.start - datetime.now()).days <= 0:
            flash("Sie können sich nicht von einem vergangenen Termin abmelden", "danger")
        else:
            zeitraum.update(pull__teilnehmer=benutzer)
            benutzer.decAngSemi()
            flash("Erfolgreich abgemeldet", "success")
        return angemeldete_seminare()
    else:
        # noinspection PyUnresolvedReferences
        return render_template("bestaetigen.html", title="Abmelden bestätigen", info=session,
                               kurs=seminar.bezeichnung, start=zeitraum.start.strftime(datetime_format_de_strf),
                               ende=zeitraum.ende.strftime(datetime_format_de_strf), id=str(zeitraum.id))


class Seminartermin(object):
    def __init__(self, bezeichnung, dozent, start, ende, id):
        self.bezeichnung = bezeichnung
        self.dozent = dozent
        self.start = start
        self.ende = ende
        self.id = id


# noinspection PyAbstractClass
class SeminartabelleAnmelden(Table):
    classes = ["table", "table-hover"]
    no_items = "Keine Seminare verfügbar"
    bezeichnung = Col("Seminar")
    dozent = Col("Dozent")
    start = DatetimeCol("Start", datetime_format=datetime_format_de)
    ende = DatetimeCol("Ende", datetime_format=datetime_format_de)
    anmelden = LinkCol("Anmelden", 'seminarbelegung.anmelden', url_kwargs=dict(id='id'))


# noinspection PyAbstractClass
class SeminartabelleAbmelden(Table):
    classes = ["table", "table-hover"]
    no_items = "Für keine Seminare angemeldet"
    bezeichnung = Col("Seminar")
    dozent = Col("Dozent")
    start = DatetimeCol("Start", datetime_format=datetime_format_de)
    ende = DatetimeCol("Ende", datetime_format=datetime_format_de)
    anmelden = LinkCol("Abmelden", 'seminarbelegung.abmelden', url_kwargs=dict(id='id'))
