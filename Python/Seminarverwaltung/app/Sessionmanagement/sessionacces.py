from functools import wraps
from flask import g, request, redirect, url_for, session, flash

from app.databaseClasses import Benutzer


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        userID=session['user_id']

        benutzer = Benutzer.objects(id=userID)[0]
        if not benutzer.check_admin():
            flash("Zugriff nur für Admins", "danger")

            next = request.args.get('next')
            return redirect(next or url_for('seminarbelegung.verfuegbare_seminare'))



        return f(*args, **kwargs)


    return decorated_function

def login_required(function_to_protect):
    @wraps(function_to_protect)
    def wrapper(*args, **kwargs):
        username = session.get('username')
        if username:
            try:
                user = Benutzer.objects.get(username=username)
            except Benutzer.objects.get(username).DoesNotExist:
                user=None

            if user:
                # Success!
                return function_to_protect(*args, **kwargs)
            else:
                flash("Session existiert, aber zugehöriger Nutzer nicht mehr", "danger")
                return redirect(url_for('Session.login'))
        else:
            flash("Sie wurden automatisch ausgelogged")
            return redirect(url_for('Session.login'))

    return wrapper
