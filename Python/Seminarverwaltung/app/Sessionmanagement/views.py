
from datetime import timedelta
from flask import Flask, render_template, Blueprint, redirect, request, url_for, session, flash
from flask_login import login_manager, login_user, logout_user, login_required, current_user
from app.Sessionmanagement.forms import LoginForm
from werkzeug.security import generate_password_hash, check_password_hash
from app.databaseClasses import Benutzer
from app.Sessionmanagement.sessionacces import login_required

session_blueprint = Blueprint("Session", __name__, template_folder="Pages")


@session_blueprint.route('/', methods=["GET", "POST"])
@session_blueprint.route('/login', methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = Benutzer.objects.get(username=form.username.data)
        login_user(user)
        session['username'] = form.username.data
        if user.admin:
            session['role'] = "admin"
        else:
            session['role'] = "nutzer"
        session['admin'] = user.admin
        session['logged_in'] = True
        flash("Erfolgreich eingelogged!", "success")
        next = request.args.get('next')
        session.permanent = True
        session.modified = True
        #HIER BITTE NUTZER STARTSEITE EINFÜGEN!
        if(session['role'] == "admin"):
            return redirect(url_for('benutzerverwaltung.benutzerliste'))
        if(session['role']=="nutzer"):
            return redirect(url_for('seminarbelegung.verfuegbare_seminare'))

    print(form.errors)
    return render_template("login.html", form=form, info=session)


@session_blueprint.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    session['logged_in'] = False
    session.pop('username', None)
    flash("Sie haben sich erfolgreich abgemeldet.", "success")
    return redirect(url_for('Session.login'))



