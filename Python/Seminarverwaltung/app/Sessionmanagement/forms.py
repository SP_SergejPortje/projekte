import pymongo
from flask import session, flash,redirect, url_for
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, ValidationError, BooleanField
from wtforms import validators
from werkzeug.security import check_password_hash

from app.databaseClasses import Benutzer



class LoginForm(FlaskForm):
    """
    The login form
    """
    username = StringField(u'Username', validators=[validators.InputRequired()])
    password = PasswordField(u'Password', validators=[validators.optional()])

    def validate(self):
        check_validate = super(LoginForm, self).validate()

        # if our validators do not pass
        if not check_validate:
            return False

        # does our user exist?
        try:
            user = Benutzer.objects.get(username=self.username.data)
        except Benutzer.DoesNotExist:
            user = None

        #check if user is stored
        if not user:
            self.username.errors.append('Benutzername oder Passwort nicht korrekt')
            return False
        #check if user is logged in already
        try:
            loggedin=session.get('logged_in')
        except session.get('logged_in').DoesNotExist:
           loggedin = None

        if loggedin==True:
            flash("Nutzer schon angemeldet Abmelden nicht vergessen!", "danger")
            return True

        #check if user is active
        if not user.aktiv:
            self.password.errors.append('Nutzer nicht aktiv')
            return False
        # do the passwords match
        if not check_password_hash(user.passwort, self.password.data):
            self.password.errors.append('Benutzername oder Passwort nicht korrekt')
            return False

        return True
