from datetime import timedelta
from flask import Flask, session
from flask_login import LoginManager
from flask_mongoengine import MongoEngine
from flask_assets import Environment
from flask_debugtoolbar import DebugToolbarExtension
from webassets.loaders import PythonLoader as PythonAssetsLoader
from app import assets
from flask_wtf import CSRFProtect

from app.databaseClasses import Benutzer
from app.Sessionmanagement.views import session_blueprint
from app.main.views import main_blueprint
from app.Auswertungen.views import auswertungen_blueprint
from app.Seminarverwaltung.views import seminarverwaltung_bp
from app.Benutzerverwaltung.views import benutzerverwaltung_blueprint
from app.Seminarbelegung.Views import seminarbelegung_blueprint


def create_app(config=None):

    # Flask App erstellen
    app = Flask("Seminarverwaltung")

    # app Konfigurieren
    app.config.from_object(config)

    #Session Lebensspanne einstellen minutes für minuten seconds für sekunden:
    app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes = 10)

    # Datenbank instanz erstellen und an app binden
    db = MongoEngine()
    db.init_app(app)

    # Asset Environment erstellen
    assets_env = Environment(app)

    # Toolbar
    debug_toolbar = DebugToolbarExtension()
    debug_toolbar.init_app(app)

    #init LoginManager
    login_manager = LoginManager()
    login_manager.login_view = "Session.login"
    login_manager.login_message = u"Bitte einloggen!"
    login_manager.login_message_category = "warning"
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return Benutzer.objects.get(pk=user_id)

    #Csrf_protection setup
    csrf = CSRFProtect()
    #init CsrfProtect
    csrf.init_app(app)

    # Assetes vom Bundle laden
    assets_loader = PythonAssetsLoader(assets)
    for name, bundle in assets_loader.load_bundles().items():
        assets_env.register(name, bundle)

    # Blueprints regestrieren
    app.register_blueprint(session_blueprint)
    app.register_blueprint(main_blueprint)
    app.register_blueprint(auswertungen_blueprint)
    app.register_blueprint(benutzerverwaltung_blueprint)
    app.register_blueprint(seminarbelegung_blueprint)
    app.register_blueprint(seminarverwaltung_bp)

    @app.before_request
    def func():
        session.modified = True

    return app

