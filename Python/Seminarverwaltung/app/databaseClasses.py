from flask_login import UserMixin
from flask_mongoengine import MongoEngine
from mongoengine import fields

# Benötigten Klassen für die MongoDb Datenbank, Klasse entspricht Collection in der Db

db = MongoEngine()


# Klasse für die Benutzer Collection
class Benutzer(UserMixin, db.Document):
    username = fields.StringField(required=True, unique=True, max_length=25)
    vorname = fields.StringField(required=True, max_length=25)
    nachname = fields.StringField(required=True, max_length=25)
    gebdat = fields.DateTimeField(required=True)
    passwort = fields.StringField(required=True)
    admin = fields.BooleanField()
    angemdeldeteSeminare = fields.IntField(default=0)
    aktiv = fields.BooleanField()

    def check_admin(self):
        return self.admin

    def incAngSemi(self):
        self.angemdeldeteSeminare += 1
        self.save()
        return self

    def decAngSemi(self):
        self.angemdeldeteSeminare -= 1
        self.save()
        return self


# Klasse für die Zeitraum Collection, beinhaltet eine Referenzen Liste auf Benutzer
class Zeitraum(db.Document):
    start = fields.DateTimeField(required=True)
    ende = fields.DateTimeField(required=True)
    teilnehmer = fields.ListField(fields.ReferenceField(Benutzer))

    def hat_teilnehmer(self, teiln_id):
        for teiln in self.teilnehmer:
            if str(teiln.id) == teiln_id:
                return True
        return False


# Klasse für die Seminar Collection, beinhaltet eine Referenzen Liste auf Zeitraum
class Seminar(db.Document):
    bezeichnung = fields.StringField(required=True, max_length=25)
    dozent = fields.StringField(required=True, max_length=25)
    zeitraeume = fields.ListField(fields.ReferenceField(Zeitraum))
    maxTeilnehmer = fields.IntField(required=True)
