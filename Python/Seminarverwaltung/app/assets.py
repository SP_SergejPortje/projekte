from flask_assets import Bundle

common_css = Bundle(
    "main/css/vendor/bootstrap.css",
    "main/css/vendor/fontawesome-all.css",
    "main/css/vendor/yearpicker.css",
    "main/css/main.css",
    "main/css/login.css",
    "main/css/auswertungen.css",
    "main/css/benutzerverwaltung.css",
    filters="cssmin",
    output="public/css/common.css"
)

common_js = Bundle(
    "main/js/vendor/jquery.js",
    "main/js/vendor/popper.js",
    "main/js/vendor/bootstrap.js",
    "main/js/vendor/yearpicker.js",
    "main/js/vendor/sortTable.js",
    Bundle(
        "main/js/main.js",
        filters="jsmin"
    ),
    output="public/js/common.js"
)