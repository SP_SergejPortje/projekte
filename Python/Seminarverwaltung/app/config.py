# Klassen zur Konfiguration der flask app

class BaseConfig(object):
    DEBUG = False
    TESTING = False


# Development Konfiguration
class DevConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    USE_RELOADER = False
    MONGODB_SETTINGS = {"db": "Seminarverwaltung", "alias": "default"}
    SECRET_KEY = "SuperSecretKey"
    DEBUG_TB_ENABLED = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False


# Testing Konfiguration
class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
    MONGODB_SETTINGS = {"db": "Seminarverwaltung", "alias": "default"}
    SECRET_KEY = "SuperSecretKey"
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    WTF_CSRF_ENABLED = False
