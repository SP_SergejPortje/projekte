from flask_table import Table, Col, DatetimeCol

datetimeFormat = "dd.MM.yyyy"


# Tabelle für die Benutzer pro Seminar Auswertung
class bpsTable(Table):
    classes = ["table", "table-hover"]
    seminar = Col("Seminar")
    dozent = Col("Dozent")
    start = DatetimeCol("Start", datetime_format=datetimeFormat)
    ende = DatetimeCol("Ende", datetime_format=datetimeFormat)
    benutzer = Col("Teilnehmer")


# Tabellenitem für die Benutzer pro Seminar Auswertung
class bpsTableItem(object):
    def __init__(self, seminar, dozent, start, ende, benutzer):
        self.seminar = seminar
        self.dozent = dozent
        self.start = start
        self.ende = ende
        self.benutzer = benutzer


# ----------------------------------------------------------------------------------------------------------------------

# Tabelle für die Auslastung pro Seminar Auswertung
class apsTable(Table):
    classes = ["table", "table-hover"]
    seminar = Col("Seminar")
    dozent = Col("Dozent")
    start = DatetimeCol("Start", datetime_format=datetimeFormat)
    ende = DatetimeCol("Ende", datetime_format=datetimeFormat)
    benutzer = Col("Teilnehmer")
    auslastung = Col("Auslastung in %")


# Tabellenitem für die Auslastung pro Seminar Auswertung
class apsTableItem(object):
    def __init__(self, seminar, dozent, start, ende, benutzer, auslastung):
        self.seminar = seminar
        self.dozent = dozent
        self.start = start
        self.ende = ende
        self.benutzer = benutzer
        self.auslastung = auslastung


# ----------------------------------------------------------------------------------------------------------------------

# Tabelle für die Seminare pro Benutzer Auswertung
class spbTable(Table):
    classes = ["table", "table-hover"]
    username = Col("Username")
    vorname = Col("Vorname")
    nachname = Col("Nachname")
    angemeldeteSeminare = Col("Anz. Belegter Seminare")


class spbTableItem(object):
    def __init__(self, username, vorname, nachname, angemeldeteSeminare):
        self.username = username
        self.vorname = vorname
        self.nachname = nachname
        self.angemeldeteSeminare = angemeldeteSeminare
