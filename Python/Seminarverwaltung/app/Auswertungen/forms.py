from flask_wtf import FlaskForm
from wtforms import DateField

#Form zum auswählen des Jahres
class YearFrom(FlaskForm):
    yearField = DateField("Jahr", format="%Y")
