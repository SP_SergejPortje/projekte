from flask import Blueprint, render_template, session
from app.Sessionmanagement.sessionacces import admin_required, login_required
from app.databaseClasses import Benutzer, Seminar
from .tables import *
from .forms import YearFrom
from datetime import datetime


# Falls Form submitted wurde hole das Jahr von der Form sonst ist Jahr das Aktuelle Kalender Jahr
def getDate(yearForm):
    if yearForm.validate_on_submit():
        return yearForm.yearField.data.year
    else:
        return datetime.now().year


# Sortiere eine Objektliste nach dem key in der in order definierten Reihenfolge(asc=Aufsteigend, desc=Absteigend)
def sortObjectList(list, key, order):
    if order == "desc":
        list.sort(key=lambda item: getattr(item, key), reverse=True)
    if order == "asc":
        list.sort(key=lambda item: getattr(item, key))


# Blueprint für die Auswertungen
auswertungen_blueprint = Blueprint("auswertungen", __name__, template_folder="Pages")


# ----------------------------------------------------------------------------------------------------------------------

@auswertungen_blueprint.route("/aswrtBenutzerProSeminar", methods=["GET", "POST"])
@login_required
@admin_required
def auswertungBenutzerProSeminar():
    form = YearFrom()
    year = getDate(form)

    tableItems = []
    # Für alle Seminare die im angegebenen Jahr liegen erstelle ein bpsTableItem (bps=Benutzer pro Seminar) und füge es
    # der Liste hinzu
    for seminar in Seminar.objects:
        for zeitraum in seminar.zeitraeume:
            if zeitraum.start.year == year and zeitraum.ende.year == year:
                tableItem = bpsTableItem(seminar.bezeichnung, seminar.dozent, zeitraum.start, zeitraum.ende,
                                         len(zeitraum.teilnehmer))
                tableItems.append(tableItem)
    # Sortiere die tableItem Liste nach Benutzern in absteigender Reihenfolge
    sortObjectList(list=tableItems, key="benutzer", order="desc")
    # Erstelle die Benutzer pro Seminar Tabelle
    table = bpsTable(tableItems)

    return render_template("BenutzerProSeminar.html", title="Seminarverwaltung - Auswertungen", info=session,
                           table=table, form=form)


# ----------------------------------------------------------------------------------------------------------------------

@auswertungen_blueprint.route("/aswrtAuslastungProSeminar", methods=["GET", "POST"])
@login_required
@admin_required
def auswertungAuslastungProSeminar():
    form = YearFrom()
    year = getDate(form)

    tableItems = []
    # Für alle Seminare die im angegebenen Jahr liegen erstelle ein apsTableItem (aps=Auslastung pro Seminar) und füge
    # es der Liste hinzu
    for seminar in Seminar.objects:
        for zeitraum in seminar.zeitraeume:
            if zeitraum.start.year == year and zeitraum.ende.year == year:
                # Berechne die Auslastung (Teilnehmer / max Teilnehmer * 100)
                auslastung = len(zeitraum.teilnehmer) / seminar.maxTeilnehmer * 100
                tableItem = apsTableItem(seminar.bezeichnung, seminar.dozent, zeitraum.start, zeitraum.ende, len(zeitraum.teilnehmer), auslastung)
                tableItems.append(tableItem)
    # Sortiere die tableItem Liste nach auslasrung in aufsteigender Reihenfolge
    sortObjectList(list=tableItems, key="auslastung", order="asc")
    # Erstelle die Auslastung pro Seminar Tabelle
    table = apsTable(tableItems)

    return render_template("AuslastungProSeminar.html", title="Seminarverwaltung - Auswertungen", info=session,
                           table=table, form=form)


# ----------------------------------------------------------------------------------------------------------------------

@auswertungen_blueprint.route("/aswrtBelSeminareProBenutzer")
@login_required
@admin_required
def auswertungBelSeminareProBenutzer():
    tableItems = []
    # Für alle aktiven Benutzer erstelle ein spbTableItem (spb=Seminare pro Benutzer) und füge es der Liste hinzu
    for benutzer in Benutzer.objects(aktiv=True):
        tableItem = spbTableItem(benutzer.username, benutzer.vorname, benutzer.nachname, benutzer.angemdeldeteSeminare)
        tableItems.append(tableItem)
    # Sortiere die tableItem Liste nach angemeldeteSeminare in absteigender Reihenfolge
    sortObjectList(list=tableItems, key="angemeldeteSeminare", order="desc")
    # Erstelle die Seminare pro Benutzer Tabelle
    table = spbTable(tableItems)

    return render_template("BelSeminareProBenutzer.html", title="Seminarverwaltung - Auswertungen", info=session,
                           table=table)
