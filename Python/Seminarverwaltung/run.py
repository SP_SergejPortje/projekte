from app import config
from app.factory import create_app

# app object erstellen
app = create_app(config.TestingConfig)

if __name__ == "__main__":
    app.run()

