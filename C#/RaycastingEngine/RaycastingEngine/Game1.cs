﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;

namespace SergeStein3D
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Vector2 playerPosition;
        private Vector2 directionVector;
        private Vector2 cameraPlane;

        private int windowHeigth;
        private int windowWidth;

        private const int textureHeigth = 160;
        private const int textrueWidth = 160;

        private SpriteFont fpsFont;
        private float frameRate;

        private int[,] worldMap;

        private struct TexturedRectangle
        {
            public Rectangle imageCoordinates;
            public Rectangle textureCoordinates;
            public Texture2D texture;
        }

        private System.Collections.Generic.List<TexturedRectangle> image;

        MouseState priviousMouseState;

        Texture2D nikolasCageTex;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            playerPosition = new Vector2(22, 12);
            cameraPlane = new Vector2(0, 0.66f);
            directionVector = new Vector2(-1, 0);

            windowHeigth = GraphicsDevice.Viewport.Height;
            windowWidth = GraphicsDevice.Viewport.Width;

            image = new System.Collections.Generic.List<TexturedRectangle>();

            Mouse.SetPosition(windowWidth / 2, windowHeigth / 2);
            priviousMouseState = Mouse.GetState();

            nikolasCageTex = Content.Load<Texture2D>("Textures\\cage");
            

            worldMap = new int[,]
            {
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,2,2,2,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1},
                {1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,3,0,0,0,3,0,0,0,1},
                {1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,2,2,0,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,4,0,0,0,0,5,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,4,0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
            }; 
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            fpsFont = Content.Load<SpriteFont>("SpriteFonts\\FPS_Font");

            Song s = Content.Load<Song>("Music\\cage2");
            MediaPlayer.Play(s);
            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            image.Clear();
            float moveSpeed = 0.20f;
            float rotSpeed = 0.01f;

            MouseState mouseState = Mouse.GetState();

            int mX = mouseState.X - priviousMouseState.X;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                if (worldMap[(int)(playerPosition.X + directionVector.X * moveSpeed), (int)(playerPosition.Y)] == 0) playerPosition.X += directionVector.X * moveSpeed;
                if (worldMap[(int)(playerPosition.X), (int)(playerPosition.Y + directionVector.Y * moveSpeed)] == 0) playerPosition.Y += directionVector.Y * moveSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                if (worldMap[(int)(playerPosition.X - directionVector.X * moveSpeed), (int)(playerPosition.Y)] == 0) playerPosition.X -= directionVector.X * moveSpeed;
                if (worldMap[(int)(playerPosition.X), (int)(playerPosition.Y - directionVector.Y * moveSpeed)] == 0) playerPosition.Y -= directionVector.Y * moveSpeed;

            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                if (worldMap[(int)(playerPosition.X + cameraPlane.X * moveSpeed), (int)(playerPosition.Y)] == 0) playerPosition.X += cameraPlane.X * moveSpeed;
                if (worldMap[(int)(playerPosition.X), (int)(playerPosition.Y + cameraPlane.Y * moveSpeed)] == 0) playerPosition.Y += cameraPlane.Y * moveSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                if (worldMap[(int)(playerPosition.X - cameraPlane.X * moveSpeed), (int)(playerPosition.Y)] == 0) playerPosition.X -= cameraPlane.X * moveSpeed;
                if (worldMap[(int)(playerPosition.X), (int)(playerPosition.Y - cameraPlane.Y * moveSpeed)] == 0) playerPosition.Y -= cameraPlane.Y * moveSpeed;

            }

            if ( mouseState != priviousMouseState)
            {
                while(mX > 0)
                {
                    //both camera direction and camera plane must be rotated
                    float oldDirX = directionVector.X;
                    directionVector.X = directionVector.X * (float)Math.Cos(-rotSpeed) - directionVector.Y * (float)Math.Sin(-rotSpeed);
                    directionVector.Y = oldDirX * (float)Math.Sin(-rotSpeed) + directionVector.Y * (float)Math.Cos(-rotSpeed);
                    float oldPlaneX = cameraPlane.X;
                    cameraPlane.X = cameraPlane.X * (float)Math.Cos(-rotSpeed) - cameraPlane.Y * (float)Math.Sin(-rotSpeed);
                    cameraPlane.Y = oldPlaneX * (float)Math.Sin(-rotSpeed) + cameraPlane.Y * (float)Math.Cos(-rotSpeed);
                    mX--;
                }

                while(mX < 0)
                {
                    //both camera direction and camera plane must be rotated
                    float oldDirX = directionVector.X;
                    directionVector.X = directionVector.X * (float)Math.Cos(rotSpeed) - directionVector.Y * (float)Math.Sin(rotSpeed);
                    directionVector.Y = oldDirX * (float)Math.Sin(rotSpeed) + directionVector.Y * (float)Math.Cos(rotSpeed);
                    float oldPlaneX = cameraPlane.X;
                    cameraPlane.X = cameraPlane.X * (float)Math.Cos(rotSpeed) - cameraPlane.Y * (float)Math.Sin(rotSpeed);
                    cameraPlane.Y = oldPlaneX * (float)Math.Sin(rotSpeed) + cameraPlane.Y * (float)Math.Cos(rotSpeed);
                    mX++;
                }
            }


            // TODO: Add your update logic here
            frameRate = 1 / (float)gameTime.ElapsedGameTime.TotalSeconds;

            for( int x = 0; x < windowWidth; x++)
            {
                float cameraX = 2 * x / (float)windowWidth - 1;
                Vector2 rayDirection = directionVector + cameraPlane * cameraX;

                int mapX = 0;
                int mapY = 0;

                mapX = (int)playerPosition.X;
                mapY = (int)playerPosition.Y;

                //Distanz zur Wand Seite
                Vector2 sideDist = new Vector2(0, 0);

                //Distanz zu den nächsten x/y Seiten
                Vector2 deltaDist;
                deltaDist.X = System.Math.Abs(1 / rayDirection.X);
                deltaDist.Y = System.Math.Abs(1 / rayDirection.Y);

                //Distanz zur Wand
                float wallDist = 0;

                //In welche Richtung gelaufen wird(positiv/negativ : 1 oder -1)
                int stepX;
                int stepY;

                //Ob Wand getroffen wurde
                bool hit = false;

                //Welche Seite getroffen wurde(x/y Seite)
                int side = -1;

                //Falls X-Komponente vom Ray negativ: stepX = -1
                if(rayDirection.X < 0)
                {
                    stepX = -1;
                    sideDist.X = (playerPosition.X - mapX) * deltaDist.X;
                }
                else
                {
                    stepX = 1;
                    sideDist.X = (mapX + 1.0f - playerPosition.X) * deltaDist.X;
                }

                //Falls Y-Komponente vom Ray negativ: stepY = -1
                if (rayDirection.Y < 0)
                {
                    stepY = -1;
                    sideDist.Y = (playerPosition.Y - mapY) * deltaDist.Y;
                }
                else
                {
                    stepY = 1;
                    sideDist.Y = (mapY + 1.0f - playerPosition.Y) * deltaDist.Y;
                }

                //DDA Algo
                while(hit == false)
                {
                    if(sideDist.X < sideDist.Y)
                    {
                        sideDist.X += deltaDist.X;
                        mapX += stepX;
                        side = 0;
                    }
                    else
                    {
                        sideDist.Y += deltaDist.Y;
                        mapY += stepY;
                        side = 1;
                    }
                    if (worldMap[mapX, mapY] > 0)
                        hit = true;
                }

                if (side == 0)
                    wallDist = (mapX - playerPosition.X + (1 - stepX) / 2) / rayDirection.X;
                else
                    wallDist = (mapY - playerPosition.Y + (1 - stepY) / 2) / rayDirection.Y;

                int lineHeight = (int)(windowHeigth / wallDist);

                int y_start = -lineHeight / 2 + windowHeigth / 2;
                if (y_start < 0)
                    y_start = 0;
                int y_end = lineHeight / 2 + windowHeigth / 2;
                if (y_end >= windowHeigth)
                    y_end = windowHeigth - 1;

                Rectangle imageRec = new Rectangle(x, y_start, 1, lineHeight);

                float wallX;

                if (side == 0)
                    wallX = playerPosition.Y + wallDist * rayDirection.Y;
                else
                    wallX = playerPosition.X + wallDist * rayDirection.X;

                wallX -= (float)Math.Floor((wallX));

                int texX = (int)(wallX * (float)textrueWidth);

                if (side == 0 && rayDirection.X > 0)
                    texX = textrueWidth - texX - 1;
                if (side == 1 && rayDirection.Y < 0)
                    texX = textrueWidth - texX - 1;

                int d1 = y_start * 256 - windowHeigth * 128 + lineHeight * 128;
                int texY_start = ((d1 * textureHeigth) / lineHeight) / 256;

                int d2 = (y_end - 1) * 256 - windowHeigth * 128 + lineHeight * 128;
                int texY_end = ((d2 * textureHeigth) / lineHeight) / 256;

                int texHeight = texY_end - texY_start;


                Rectangle texRec = new Rectangle(texX, texY_start, 1, texHeight);

                image.Add(new TexturedRectangle() { imageCoordinates = imageRec, textureCoordinates = texRec, texture = nikolasCageTex });
            }

            Mouse.SetPosition(windowWidth / 2, windowHeigth / 2);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            foreach (TexturedRectangle rec in image)
            {
                spriteBatch.Draw(rec.texture, rec.imageCoordinates, rec.textureCoordinates, Color.White);
            }

            spriteBatch.DrawString(fpsFont, "FPS: " + frameRate, new Vector2(0, 0), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
